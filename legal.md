---
layout: page
title: Privacy Policy
background: grey
---
<div class="col-lg-12 text-center">
	<h2 class="section-heading text-uppercase">Privacy Policy</h2>
</div>

This Privacy Policy describes how your personal information is collected, used, and shared when you visit {{ site.title }} website or use our apps.

**How long we keep your Personal Data**

We will not keep your Personal Data for any longer than we think is necessary.
When deciding how long to keep your Personal Data, we consider factors including:
our contractual obligations and rights in relation to the Personal Data involved (including the End-User Terms of Service);
legal obligation(s) under applicable law to retain data for a certain period of time;
whether we relied on your consent to use the Personal Data, but you have since withdrawn your consent;
statute of limitations under applicable law(s);
our legitimate interests where we have carried out balancing tests;
fraud and risk management;
(potential) disputes; and
guidelines issued by relevant data protection authorities.
**Sharing of your Personal Data**
We may also share your Personal Data:
if we reasonably consider that we are under a duty to disclose or share your Personal Data in order to comply with any legal obligation;
we need to enforce or apply our End-User Terms of Service and other agreements;
to protect the rights, property, or safety of TrueLayer, our customers, or others;
if we have to do so to fulfil our legal obligations;
with partners or suppliers who process Personal Data on our behalf (such as our professional advisers, payment schemes, auditors or IT suppliers) - we take the security and protection of your Personal Data seriously and only allow such suppliers to use your Personal Data for specified purposes and in accordance with our instructions;
with third parties to whom we may sell, transfer or merge parts of our business or assets. If a change like this happens to our business, the new owners may use your Personal Data in the same way as set out in this Privacy Policy; and/or
to another company in our group, if this is necessary to ensure continuity in the provision of Services to you (including in relation to the UK’s withdrawal from the European Union), or to reflect any business reorganisation or expansion that we may engage in from time to time.

**CHANGES**

We may update this privacy policy from time to time for personal, operational, legal, or regulatory reasons.

**CONTACT US**

For more information about our privacy practices or if you have questions, please contact us by email at <a href="mailto:{{ site.email }}">{{ site.email }}</a>.
